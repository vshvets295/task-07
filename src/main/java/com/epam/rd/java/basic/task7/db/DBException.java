package com.epam.rd.java.basic.task7.db;

public class DBException extends Exception {
	public String message;
	public Throwable cause;

	public DBException(String message, Throwable cause) {
		this.message = message;
		this.cause = cause;
	}
}
